# coding=utf-8
import collections
from unittest import mock
import builtins


def get_str(recommendation):
    """
    Функция проверки ввода.

    Функция для проверки ввода. Принимает на вход строку. В случае, если значение не является строкой, состоящей из
    заглавных латинских букв, вызывается сообщение об ошибке Incorrect Value.
    Args:
        recommendation
    Returns:
        value: Строка
    Raises:
        Не генерирует исключений.
    Examples:
        >>> get_str('ABCD')
        'ABCD"
        >>>get_str('hbd')
        Incorrect Value
    """
    while True:
        latin_letters = 'QWERTYUIOPASDFGHJKLZXCVBNM'
        value = input(recommendation)
        for i in range(len(value)):
            if value[i] in latin_letters:
                return value
        print('Incorrect Value')


def test_1():
    """
    Функция проверяет корректность работы основной фукнции проверки на ввод.

    Включает в себя значение, которое подставляет в основную функцию и прверяет результат со своим изначально прописанным,
    если эти значения совпадают, то выводится сообщение, что тест пройден.

    Args:
        self
    Returns:
        Ничего не возвращает.
    Raises:
        Не генерирует исключений.
    """
    with mock.patch.object(builtins, 'input', lambda _: 'DDDGHH'):
        assert get_str('DDDGHH') == 'DDDGHH'


def search_for_a_trigrams(str_):
    """
    Функция принимает на вход строку, затем ищет в ней триграммы и определяет, какая имеет больше всего повторений.

    Если таких несколько - выводятся все триграммы.

    Args:
        str_: Строка
    Returns:
        Триграмма, которая имеет наибольшее количество повторений в строке.
    Raises:
        Не генерирует исключений.
    Examples:
        >>>'BBCDDD'
        'DDD'
        >>>"ABHDFG"
        'ABH BHD HDF DFG'
    """
    max_number = 0
    d = {}
    for i in range(1, len(str_)):
        key = str_[(i - 1):(i + 2)]
        key = ''.join(sorted(key))
        if key in d:
            d[key] += 1
        else:
            d[key] = 1
    print(d)
    [last] = collections.deque(d, maxlen=1)
    if len(last) == 2:
        del d[last]
    for i in d:
        if d[i] > max_number:
            max_number = d[i]
    for i in d:
        if d[i] == max_number:
            print(i)


def test_3(capsys):
    """
    Функция проверяет корректность работы основной фукнции поиска триграммы в строке. Включает в себя значение, которое
    подставляет в основную функцию и прверяет результат со своим изначально прописанным, если эти значения совпадают,
    то выводится сообщение, что тест пройден.

    Args:
        capsys
    Returns:
        Ничего не возвращает.
    Raises:
        Не генерирует исключений.
    """
    search_for_a_trigrams("DDDHGFHJDDD")
    out, err = capsys.readouterr()
    assert out == "DDD\n"


def test_5(capsys):
    """
    Функция проверяет корректность работы основной фукнции поиска триграммы в строке.

    Включает в себя значение, которое подставляет в основную функцию и прверяет результат со своим изначально прописанным,
    если эти значения совпадают, то выводится сообщение, что тест пройден.

    Args:
        capsys
    Returns:
        Ничего не возвращает.
    Raises:
        Не генерирует исключений.
    """
    search_for_a_trigrams('ABCDEF')
    out, err = capsys.readouterr()
    assert out == 'ABC\nBCD\nCDE\nDEF\n'


def main():
    str_ = get_str('Enter the string: ')
    search_for_a_trigrams(str_)


if __name__ == "__main__":
    main()
